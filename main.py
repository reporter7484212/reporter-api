import tempfile
from datetime import datetime

import requests
from flask import Flask, request, jsonify, json
from flask_cors import CORS
import csv
import subprocess
from speaker_diarization import diarization
from speaker_enrollement import enrollement
from speaker_identification import identification
from download_audio import download_audio
from delete_file import delete_file
import speech_to_text

app = Flask(__name__)
CORS(app)

#// Doucet, Charles-Etienne
@app.route("/")
def home():
    return "Home"

#// Doucet, Charles-Etienne
# get all the lines a person said ex: http://.../get-person/Sarah
@app.route("/get-person/<person_Id>")
def get_person(person_Id):
    response = []
    data=[]
    with open("chat_data.csv") as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            data.append(row)
        #give a list of all in col
    col = [x[2].lower() for x in data]
    if person_Id.lower() in col:
        for x in range(0,len(data)):
            if person_Id.lower() in data[x][2].lower():
                response.append(data[x])
    else:
        print("Name doesnt exist")

    return jsonify(response),200

#// Doucet, Charles-Etienne
# get all the lines a person said ex: http://.../get-person/2002-08-02
@app.route("/get-date/<date>")
def get_date(date):
    response = []
    data=[]
    with open("chat_data.csv") as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            data.append(row)
        #give a list of all in col
    col = [x[0] for x in data]
    if date in col:
        for x in range(0,len(data)):
            if date in data[x][0]:
                response.append(data[x])
    else:
        print("Nothing was said on this date")

    return jsonify(response),200

@app.route("/get-data-filtered/", methods=["POST"])
def get_data():
    request_data = request.json
    date = request_data.get("date", "")
    name = request_data.get("name", "")
    meeting = request_data.get("meeting", "")
    files_url = request_data.get("url")

    response = []

    for url in files_url:
        try:
            csv_response = requests.get(url)
            csv_data = csv_response.text.splitlines()
            reader = csv.reader(csv_data)
            for row in reader:
                print(row)
                if (
                    date == "" or date in row[0]
                ) and (
                    name == "" or name.lower() in row[1].lower()
                ):
                    response.append(row)
        except Exception as e:
            return jsonify({"error": str(e)}), 500

    return jsonify(response), 200


#// Doucet, Charles-Etienne
# route post new text ex: http://.../post-text  body{date:"", name:"", text:""}
@app.route("/post-text/", methods=["POST"])
def update_text():
    data = request.get_json()
    text = json.loads(request.data)
    csv_row = [text["Date"], text["name"],'"'+text["text"]+'"']
    print(csv_row)
    # Open the CSV file in append mode ('a')
    with open('chat_data.csv', 'a') as fd:
        # Write the new row
        fd.write('\n'+','.join(csv_row))
    return jsonify(data),201


#// Gagné, Julien
@app.route("/speakers/")
def get_speakers():
    speakers = []
    data=[]
    with open("chat_data.csv") as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            data.append(row)
        if len(data):
            for x in range(1,len(data)):
                if len(speakers) > 0:
                    if data[x][2] in speakers:
                        continue
                    else:
                        speakers.append(data[x][2])
                else:
                    speakers.append(data[x][2])
            return jsonify(speakers), 200
        else:
            return jsonify("No speakers found"), 404


@app.route("/meetings/")
def get_meetings():
    meetings = []
    data=[]
    with open("chat_data.csv") as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            data.append(row)
        if len(data):
            for x in range(1,len(data)):
                if len(meetings) > 0:
                    if data[x][1] in meetings:
                        continue
                    else:
                        meetings.append(data[x][1])
                else:
                    meetings.append(data[x][1])
            return jsonify(meetings), 200
        else:
            return jsonify("No meetings found"), 404

#// Gagné, Julien
@app.route("/segment-audio/", methods=["POST"])
def segment_audio():
    # Get audio URL and users from the request body
    request_data = request.json  # Assuming JSON is sent in the request body
    audio_url = request_data.get("audio_url")

    # Download the audio
    audio = download_audio(audio_url)

    # Process the audio
    if audio:
        speaker_segments = diarization(audio)

        return jsonify([speaker_segments, audio]), 201
    else:
        return jsonify({"error": "Failed to download audio"}), 500

@app.route("/identification-audio/", methods=["POST"])
def identification_audio():
    # Get audio URL and users from the request body
    request_data = request.json  # Assuming JSON is sent in the request body
    users = request_data.get("users")
    speaker_segments = request_data.get("speaker_segments")
    audio = request_data.get("audio")

    # Process the audio
    if audio:
        profiles = enrollement(users)
        users_identification = identification(audio, speaker_segments, profiles)
        return jsonify(users_identification), 201
    else:
        return jsonify({"error": "Failed to download audio"}), 500

@app.route("/process-audio/", methods=["POST"])
def process_audio():
    # Get audio URL and users from the request body
    request_data = request.json  # Assuming JSON is sent in the request body
    users_identification = request_data.get("users_identification")
    audio = request_data.get("audio")

    # Process the audio
    if audio:
        users_transcription = speech_to_text.record_text(audio, users_identification)
        data = []

        for transcription in users_transcription:
            name = transcription["name"]
            text = transcription["text"]
            if len(text) >= 1:
                date = get_current_date()
                data.append({"date": date, "name": name, "text": text})
        delete_file(audio)
        return jsonify(data), 201

    else:
        return jsonify({"error": "Failed to download audio"}), 500

# get the record python script to run http://.../record
#// Doucet, Charles-Etienne
@app.route("/record")
def recording():
  # Execute your Python script
    try:
        result = subprocess.check_output(['python', 'speech_to_text.py'])
        return result.decode('utf-8')
    except Exception as e:
        return str(e)
# stop recording
#// Doucet, Charles-Etienne
@app.route('/close_app', methods=['GET'])
def close_app():
    shutdown_server()
    return jsonify({'message': 'Server shutting down...'})

def shutdown_server():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()
if __name__ == "__main__":
    app.run(debug=True)


def get_current_date():
    # Get current date
    current_date = datetime.now()
    # Format date as YYYY-MM-DD
    formatted_date = current_date.strftime('%Y-%m-%d')
    return formatted_date