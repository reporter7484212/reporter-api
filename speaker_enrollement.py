#// Gagné, Julien
import pveagle
import datetime
from download_audio import download_audio
from delete_file import delete_file

access_key = "fq1/ZJgsyGIXKcl+ATzQia4wY+XNGeQs5jrG0uB3d0tuSyfDBH3BMQ=="


def read_audio_file(file_path):
    with open(file_path, "rb") as f:
        audio_data = f.read()
    return audio_data


def enrollement(users):
    profiles = []

    # Création des profile pour l'identification des utilisateurs
    for user in users:
        eagle_profiler = pveagle.create_profiler(access_key)
        percentage = 0.0
        filepath = download_audio(user["URL"])
        audio_data = read_audio_file(filepath)
        while percentage < 100.0:
            percentage, feedback = eagle_profiler.enroll(audio_data)
            timestamp = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            print(f"At {timestamp}, {feedback.name}: {percentage}%")
        # Exportation du profile
        speaker_profile = eagle_profiler.export()
        # Create an instance of Eagle for each speaker profile
        eagle = pveagle.create_recognizer(access_key, speaker_profile)
        # Stockage du profile et du nom de l'utilisateur dans une liste
        profiles.append({"name": user["name"], "recognizer": eagle})
        eagle_profiler.delete()
        print(profiles)
        delete_file(filepath)
    return profiles