import os
import string
import tempfile
from datetime import time
from random import random

from firebase_admin import storage


def generate_random_file_name():
    """Generate a random file name."""
    timestamp = str(int(time.time() * 1000))  # Current timestamp in milliseconds
    random_string = ''.join(random.choices(string.ascii_lowercase + string.digits, k=6))  # Random string of length 6
    return f"{timestamp}{random_string}"


async def upload_file(file):
    """Upload a file to Firebase Storage."""
    bucket = storage.bucket()
    blob = bucket.blob(f"csv/{generate_random_file_name()}.csv")

    try:
        # Upload file with Content-Type metadata set to text/csv
        blob.upload_from_file(file, content_type="text/csv")

        # Get download URL
        download_url = blob.generate_signed_url(expires_in=3600)  # 1 hour expiration
        return download_url
    except Exception as e:
        print("Error uploading file:", e)
        raise e  # Propagate the error to the caller


async def add_meeting(users, title, audio_url, file_path, company_id):
    """Add meeting data to the database."""
    try:
        # Upload file
        file_url = await upload_file(file_path)

        # Get company ID (assuming you have a function for this)

        # Set meeting data in the database
        # Replace the following lines with your database logic
        meeting_data = {
            "title": title,
            "audio_url": audio_url,
            "file_url": file_url,
            "speakers": users
        }
        # Insert the meeting data into your database
        print("Meeting added successfully")
    except Exception as e:
        # Handle errors
        print("Error adding meeting:", e)
        # You can re-raise the error if you want to propagate it to the caller
        # raise e