from speaker_diarization import diarization
from speaker_enrollement import enrollement
from speaker_identification import identification
from download_audio import download_audio
from delete_file import delete_file
import speech_to_text

users = [{"name":"Julien", "url": "https://firebasestorage.googleapis.com/v0/b/test-the-reporter.appspot.com/o/Enregistrement_3.wav?alt=media&token=09398c49-cd2e-48e2-8a48-af4be9dd3ea6"}, {"name":"Michael", "url": "https://firebasestorage.googleapis.com/v0/b/test-the-reporter.appspot.com/o/Enregistrement_6.wav?alt=media&token=7075efa6-b756-48e1-a59d-cc8aabc38098"}]
audio_url = "https://firebasestorage.googleapis.com/v0/b/test-the-reporter.appspot.com/o/Enregistrement_9.wav?alt=media&token=51957aab-51f7-4f64-a37f-75a1147408b4"
title = "meeting_test"

audio = download_audio(audio_url)
speaker_segments = diarization(audio)
profiles = enrollement(users)
users_identification = identification(audio, speaker_segments, profiles)
users_transcription = speech_to_text.record_text(audio, users_identification)
speech_to_text.output_text(title, users_transcription)
delete_file(audio)

