#// Doucet, Charles-Etienne
import sounddevice as sd
import numpy as np
import scipy.io.wavfile as wav

# Set parameters
duration = 0.035  # Duration of audio to record in seconds
samplerate = 1650  # Sampling rate (samples per second)
filename = "recorded_audio.wav"  # Name of the output .wav file

# Record audio
print("Recording...")
audio_data = sd.rec(int(duration * samplerate), samplerate=samplerate, channels=1, dtype='float64')
sd.wait()  # Wait for recording to complete
print("Recording finished.")

# Save recorded audio to a .wav file
wav.write(filename, samplerate, np.array(audio_data))

print(f"Audio saved as {filename}.")
