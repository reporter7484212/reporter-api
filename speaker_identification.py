#// Gagné, Julien

import wave
import numpy as np


access_key = "fq1/ZJgsyGIXKcl+ATzQia4wY+XNGeQs5jrG0uB3d0tuSyfDBH3BMQ=="


def read_audio_frames(file_path, start_time, end_time, frame_length=512):
    audio_frames = []
    with wave.open(file_path, 'rb') as wf:
        frame_rate = wf.getframerate()
        start_sample_index = int(start_time * frame_rate)
        end_sample_index = int(end_time * frame_rate)
        # Seek to the start position
        wf.setpos(start_sample_index)
        while wf.tell() < end_sample_index:
            frame = wf.readframes(frame_length)
            # If not enough frames left, break the loop
            if not frame:
                break
            # Convert the frame to a numpy array
            frame_np = np.frombuffer(frame, dtype=np.int16)
            # If the frame length is less than 512, pad it with zeros
            if len(frame_np) < frame_length:
                frame_np = np.pad(frame_np, (0, frame_length - len(frame_np)))
            # If the frame length is greater than 512, truncate it
            elif len(frame_np) > frame_length:
                frame_np = frame_np[:frame_length]
            audio_frames.append(frame_np)
    return audio_frames, frame_rate


def identification(audio_url, speaker_segments, profiles): # users = [{"name":"Julien", "filePath": "D:/Downloads/Enregistrement_3.wav"}, {"name":"Michael", "filePath": "D:/Downloads/Enregistrement_6.wav"}]
    user_segments = []
    for speaker, segments in speaker_segments.items():

        highestScore = {}
        for profile in profiles:
            userScore = 0
            nbSegments = len(segments)
            for segment in segments:
                start_time = segment["start"]
                end_time = segment["end"]
                audio_frames, frame_rate = read_audio_frames(audio_url, start_time, end_time)

                totalSegmentScore = 0
                iteration = 0
                print(profile)
                recognizer = profile["recognizer"]
                for i, frame in enumerate(audio_frames):
                    scores = recognizer.process(frame)
                    if scores[0] < 1:
                        iteration += 1
                        totalSegmentScore += scores[0]
                if iteration > 0:
                    segmentScore = totalSegmentScore/iteration
                else:
                    segmentScore = 0
                if segmentScore > userScore:
                    userScore = segmentScore
                print("total", segmentScore)

            userScore = userScore/nbSegments
            if highestScore:
                if userScore > highestScore["score"]:
                    highestScore = {"name": profile["name"], "score": userScore }
            else:
                highestScore = {"name": profile["name"], "score": userScore}

        name = highestScore["name"]
        for segment in segments:
            user_segments.append({"name": name, "start": segment["start"], "end": segment["end"]})
    print(user_segments)
    return sorted(user_segments, key=lambda x: x["start"])