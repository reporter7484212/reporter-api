#// Gagné, Julien
import pvfalcon

access_key = "fq1/ZJgsyGIXKcl+ATzQia4wY+XNGeQs5jrG0uB3d0tuSyfDBH3BMQ=="

def diarization(audio):
    falcon = pvfalcon.create(access_key)
    # Process audio file
    segments = falcon.process_file(audio)

    # Define an empty dictionary to store speaker segments
    speaker_segments = {}

    # Organize segments into speaker_segments dictionary
    for segment in segments:
        speaker_tag = segment.speaker_tag
        segment_info = {"start": segment.start_sec, "end": segment.end_sec}

        if speaker_tag not in speaker_segments:
            speaker_segments[speaker_tag] = []

        speaker_segments[speaker_tag].append(segment_info)

    print(speaker_segments)

    # Free resources
    falcon.delete()
    return(speaker_segments)