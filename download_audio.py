#// Gagné, Julien
import requests
import os


def download_audio(url):
    try:
        # Fetch audio data from the URL
        response = requests.get(url)
        if response.status_code == 200:
            # Extract file name from URL
            file_name = url.split('/')[-1]
            file_name = file_name.split("?")[0]
            print(file_name)
            save_path = os.path.join(file_name)

            # Save audio data to the specified path
            with open(save_path, 'wb') as audio_file:
                audio_file.write(response.content)
            print("Audio downloaded successfully to:", save_path)
            return save_path
        else:
            print("Failed to download audio: ", response.status_code)
            return None
    except Exception as e:
        print("Error:", e)
        return None