import csv

with open("chat_data.csv", 'r') as csv_file:
    reader = csv.reader(csv_file)
    header = next(reader)  # Skip the first line (header)
    rows = list(reader)

# Modify the date in each row
for row in rows:
    date = row[0].split("/")
    print(date)
    if int(date[0]) <= 9:
        month = "0" + date[0]
    else:
        month = date[0]
    if int(date[1]) <=9:
        day = "0" + date[1]
    else:
        day = date[1]

    row[0] = date[2] + "-" + month + "-" + day

# Write the modified contents back to the original file
with open("chat_data.csv", 'w', newline='') as csv_file:
    writer = csv.writer(csv_file)
    writer.writerow(header)
    writer.writerows(rows)