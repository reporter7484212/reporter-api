import os
import tempfile

import speech_recognition as sr
from datetime import datetime


# Initialize the recognizer

# get current date

#// Doucet, Charles-Etienne
def get_current_date():
    # Get current date
    current_date = datetime.now()
    # Format date as YYYY-MM-DD
    formatted_date = current_date.strftime('%Y-%m-%d')
    return formatted_date

#// Doucet, Charles-Etienne
#// Gagné, Julien
def record_text(audio_url, user_segments):
    recognizer = sr.Recognizer()
    users_transcription = []

    for segment in user_segments:
        name = segment["name"]
        start = segment["start"]
        end = segment["end"]
        duration = end - start
        with sr.AudioFile(audio_url) as source:
            audio_data = recognizer.record(source, offset=start, duration=duration)  # Record the entire audio file

        try:
            # Recognize the audio using Google Speech Recognition
            text = recognizer.recognize_google(audio_data, language ="fr-CA")
            users_transcription.append({"name": name, "text": text})
        except sr.UnknownValueError:
            print("Sorry, I could not understand what was said.")
            users_transcription.append({"name": name, "text": "Incompréhensible"})
        except sr.RequestError as e:
            print(f"Error fetching results from Google Speech Recognition service: {e}")
            users_transcription.append({"name": name, "text": "Erreur lors de L'appel vers google"})

    return users_transcription

#// Doucet, Charles-Etienne
#// Gagné, Julien
def output_text(users_transcription):
    data = []

    # Create a temporary file to store CSV data
    with tempfile.NamedTemporaryFile(mode='w', delete=False, suffix='.csv') as temp_csv:
        temp_csv.write("Date,Name,Text\n")  # Writing header to the CSV file

        # Write transcription data to the CSV file
        for transcription in users_transcription:
            name = transcription["name"]
            text = transcription["text"]
            if len(text) >= 1:
                date = get_current_date()
                temp_csv.write(f"{date},{name},{text}\n")
                data.append({"date": date, "name": name, "text": text})

    # At this point, the temporary CSV file has been created and written to
    print(data)
    return data